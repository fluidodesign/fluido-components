import animejs from 'animejs'

const defaultProps = {
  container: 'window', // or 'parent' or HTMLElement
  orientation: 'vertical', // or 'horizontal'
}

const scrollTriggerProps = {
  ...defaultProps,
  enterOffset: 0,
  exitOffset: 0,
  enterAnimationStart: 'before-enter', // or 'inside-bounds'
  exitAnimationStart: 'before-exit', // or 'outside-bounds'
  enterAnimation: null,
  exitAnimation: null,
  enterAnimationNative: null,
  exitAnimationNative: null,
}

const scrollProgressProps = {
  ...defaultProps,
  precision: 2,
  offsetStart: 0,
  offsetEnd: 0,
  start: 'entering', // or 'full-visible'
  end: 'before-leaving', // or 'after-leaving',
  animation: null,
  animationNative: null,
}

const scrollLimitProps = {
  ...defaultProps,
  precision: 2,
  start: 0,
  end: 0,
  animation: null,
  animationNative: null,
}

function getBoundingState(start, end, box, precision) {
  const res = {}
  res.start = start < box
  res.end = end < 0
  res.visible = res.start && !res.end

  if (precision) {
    if (start >= box) {
      res.progress = 0
    } else {
      let distance = end - start + box
      let temp = 1 - end / distance
      const p = Math.pow(10, precision)
      res.progress = Math.round(temp * p) / p
    }
  }

  return res
}

function getBoundingOrientation(orientation, container, node) {
  const res = {}
  let b = node.getBoundingClientRect()

  if (orientation === 'horizontal') {
    res.start = b.left
    res.size = b.width
    res.end = b.left + b.width
    res.box = container.innerWidth || container.offsetWidth
  } else if (orientation === 'vertical') {
    res.start = b.top
    res.size = b.height
    res.end = b.top + b.height
    res.box = container.innerHeight || container.offsetHeight
  } else {
    throw new Error('the orientation can only be "horizontal" and "vertical"')
  }
  return res
}

function animeRemapTargets(node, targets) {
  if (typeof targets === 'string') {
    return node.querySelectorAll(targets)
  } else if (Array.isArray(targets)) {
    let res = []
    targets.forEach(el => {
      res.concat(...animeRemapTargets(node, el))
    })
    return res
  } else {
    return [targets]
  }
}

function animationFactory(call, node) {
  let animObj = call(animejs, node, animeRemapTargets)
  if (!Array.isArray(animObj)) {
    animObj = [animObj]
  }
  return animObj
}

export function scrollTrigger(node, prop) {
  let {
    container,
    orientation,
    enterOffset,
    exitOffset,
    enterAnimationStart,
    exitAnimationStart,
    enterAnimation,
    exitAnimation,
    enterAnimationNative,
    exitAnimationNative,
  } = { ...scrollTriggerProps, ...prop }

  const triggerDirt = {
    start: false,
    end: false,
  }

  const computeContainer =
    container === 'window'
      ? window
      : container === 'parent'
      ? node.parentNode
      : container

  if (
    !computeContainer instanceof HTMLElement &&
    !computeContainer instanceof Window
  )
    throw new Error('the container must be an "HTMLElement" or the "window"')

  if (typeof enterOffset !== 'number' || typeof exitOffset !== 'number')
    throw new Error('fields [enterOffset, exitOffset] must contain numbers')

  function handleScroll() {
    let bounds = getBoundingOrientation(orientation, computeContainer, node)
    if (enterAnimationStart === 'inside-bounds') {
      bounds.start += bounds.size
    } else if (enterAnimationStart !== 'before-enter') {
      throw new Error(
        'the enterAnimationStart field must contain the states of "before-enter" or "inside-bounds"'
      )
    }

    if (exitAnimationStart === 'before-exit') {
      bounds.end -= bounds.size
    } else if (exitAnimationStart !== 'outside-bounds') {
      throw new Error(
        'the exitAnimationStart field must contain the states of "outside-bounds" or "before-exit"'
      )
    }

    bounds.start += enterOffset
    bounds.end += exitOffset

    let state = getBoundingState(bounds.start, bounds.end, bounds.box)

    if (typeof enterAnimationNative === 'function' && !triggerDirt.enterCb) {
      triggerDirt.enterCb = p => enterAnimationNative(node, p)
    } else if (typeof enterAnimation === 'function' && !triggerDirt.enterCb) {
      triggerDirt.enterAnimejs = animationFactory(enterAnimation, node)
      triggerDirt.enterCb = p => {
        triggerDirt.enterAnimejs.forEach(ajs => {
          if (
            (p && ajs.direction === 'reverse') ||
            (!p && ajs.direction === 'normal')
          ) {
            ajs.reverse()
          }
          if (ajs.paused) ajs.play()
        })
      }
    }

    if (typeof exitAnimationNative === 'function' && !triggerDirt.exitCb) {
      triggerDirt.exitCb = p => exitAnimationNative(node, p)
    } else if (typeof exitAnimation === 'function' && !triggerDirt.exitCb) {
      triggerDirt.exitAnimejs = animationFactory(exitAnimation, node)
      triggerDirt.exitCb = p => {
        triggerDirt.exitAnimejs.forEach(ajs => {
          if (
            (p && ajs.direction === 'reverse') ||
            (!p && ajs.direction === 'normal')
          ) {
            ajs.reverse()
          }
          if (ajs.paused) ajs.play()
        })
      }
    } else if (exitAnimation === 'reverse' && !triggerDirt.exitCb) {
      triggerDirt.exitCb = p => triggerDirt.enterCb(!p)
    }

    if (triggerDirt.start ? !state.start : state.start) {
      if (triggerDirt.enterCb) triggerDirt.enterCb(state.start)
    }
    if (triggerDirt.end ? !state.end : state.end) {
      if (triggerDirt.exitCb) triggerDirt.exitCb(state.end)
    }

    triggerDirt.start = state.start
    triggerDirt.end = state.end
  }

  computeContainer.addEventListener('scroll', handleScroll)
  return {
    destroy() {
      computeContainer.removeEventListener('scroll', handleScroll)
    },
  }
}

export function scrollProgress(node, prop) {
  let {
    container,
    orientation,
    offsetStart,
    offsetEnd,
    precision,
    start,
    end,
    animation,
    animationNative,
  } = { ...scrollProgressProps, ...prop }

  const triggerDirt = {}

  const computeContainer =
    container === 'window'
      ? window
      : container === 'parent'
      ? node.parentNode
      : container

  if (
    !computeContainer instanceof HTMLElement &&
    !computeContainer instanceof Window
  )
    throw new Error('the container must be an "HTMLElement" or the "window"')

  if (
    typeof offsetStart !== 'number' ||
    typeof offsetEnd !== 'number' ||
    typeof precision !== 'number'
  )
    throw new Error(
      'fields [offsetStart, offsetEnd, precision] must contain numbers'
    )

  function handleScroll() {
    let bounds = getBoundingOrientation(orientation, computeContainer, node)

    if (start === 'full-visible') {
      bounds.start += bounds.size
    } else if (start !== 'entering') {
      throw new Error(
        'the "start" field must contain the states of "entering" or "full-visible"'
      )
    }

    if (end === 'before-leaving') {
      bounds.end -= bounds.size
    } else if (end !== 'after-leaving') {
      throw new Error(
        'the "end" field must contain the states of "after-leaving" or "before-leaving"'
      )
    }

    bounds.start += offsetStart
    bounds.end += offsetEnd

    let state = getBoundingState(
      bounds.start,
      bounds.end,
      bounds.box,
      ~~precision
    )

    if (state.progress < 0) state.progress = 0
    if (state.progress > 1) state.progress = 1

    if (typeof animationNative === 'function' && !triggerDirt.cb) {
      triggerDirt.cb = p => animationNative(node, p)
    } else if (typeof animation === 'function' && !triggerDirt.cb) {
      triggerDirt.animejs = animationFactory(animation, node)
      triggerDirt.cb = p =>
        triggerDirt.animejs.forEach(ajs => ajs.seek(ajs.duration * p))
    }

    triggerDirt.cb(state.progress)
  }

  computeContainer.addEventListener('scroll', handleScroll)
  return {
    destroy() {
      computeContainer.removeEventListener('scroll', handleScroll)
    },
  }
}

export function scrollFixed(node, prop) {
  let {
    container,
    orientation,
    precision,
    start,
    end,
    animation,
    animationNative,
  } = { ...scrollLimitProps, ...prop }

  const triggerDirt = {}

  const computeContainer =
    container === 'window'
      ? window
      : container === 'parent'
      ? node.parentNode
      : container

  if (
    !computeContainer instanceof HTMLElement &&
    !computeContainer instanceof Window
  )
    throw new Error('the container must be an "HTMLElement" or the "window"')

  if (
    typeof start !== 'number' ||
    typeof end !== 'number' ||
    typeof precision !== 'number'
  )
    throw new Error('fields [start, end, precision] must contain numbers')

  function handleScroll() {
    let bounds = {
      start,
      end,
    }

    if (orientation === 'horizontal') {
      bounds.box = container.innerHeight || container.offsetHeight
    } else if (orientation === 'vertical') {
      bounds.box = container.innerWidth || container.offsetWidth
    } else {
      throw new Error('the orientation can only be "horizontal" and "vertical"')
    }

    let state = getBoundingState(
      bounds.start,
      bounds.end,
      bounds.box,
      ~~precision
    )

    if (state.progress < 0) state.progress = 0
    if (state.progress > 1) state.progress = 1

    if (typeof animationNative === 'function' && !triggerDirt.cb) {
      triggerDirt.cb = p => animationNative(node, p)
    } else if (typeof animation === 'function' && !triggerDirt.cb) {
      triggerDirt.animejs = animationFactory(animation, node, true)
      triggerDirt.cb = p =>
        triggerDirt.animejs.forEach(ajs => ajs.seek(ajs.duration * p))
    }

    triggerDirt.cb(state.progress)
  }

  computeContainer.addEventListener('scroll', handleScroll)
  return {
    destroy() {
      computeContainer.removeEventListener('scroll', handleScroll)
    },
  }
}
