import r from './ripple.mjs'
import * as s from './scrollAnimation.mjs'

export const ripple = r
export const scrollTrigger = s.scrollTrigger
export const scrollProgress = s.scrollProgress
export const scrollFixed = s.scrollFixed
