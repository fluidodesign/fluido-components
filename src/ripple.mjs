const css = `
.rippleJS {
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: hidden;
  border-radius: inherit;
  -webkit-mask-image: -webkit-radial-gradient(circle, white, black);
}

.rippleJS.fill::after {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  content: "";
}
.rippleJS.fill {
  border-radius: 1000000px;
}

.rippleJS .ripple {
  position: absolute;
  border-radius: 100%;
  background: var(--ripple-color, currentColor);
  opacity: 0.2;
  width: 0;
  height: 0;

  -webkit-transition: -webkit-transform var(--ripple-duration, 0.4s) ease-out, opacity var(--ripple-duration, 0.4s) ease-out;
  transition: transform var(--ripple-duration, 0.4s) ease-out, opacity var(--ripple-duration, 0.4s) ease-out;

  -webkit-transform: scale(0);
  transform: scale(0);

  pointer-events: none;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.rippleJS .ripple.held {
  opacity: 0.4;
  -webkit-transform: scale(1);
  transform: scale(1);
}

.rippleJS .ripple.done {
  opacity: 0.0;
}
`
const rippleTypeAttr = 'data-event'

function hasCSS() {
  let test = document.createElement('div')
  test.className = 'rippleJS'
  document.body.appendChild(test)
  let s = window.getComputedStyle(test)
  let result = s.position === 'absolute' && s.overflow === 'hidden'
  document.body.removeChild(test)
  return result
}

function setupCss() {
  if (!hasCSS()) {
    let style = document.createElement('style')
    style.textContent = css
    document.head.insertBefore(style, document.head.firstChild)
  }
}

setupCss()

function startRipple(type, at, holder, center, duration) {
  if (!holder && !holder.parentNode) {
    return false // ignore
  }
  let cl = holder.classList

  // Store the event use to generate this ripple on the holder: don't allow
  // further events of different types until we're done. Prevents double-
  // ripples from mousedown/touchstart.
  let prev = holder.getAttribute(rippleTypeAttr)
  if (prev && prev !== type) {
    return false
  }
  holder.setAttribute(rippleTypeAttr, type)

  // Create and position the ripple.
  let rect = holder.getBoundingClientRect()
  let x
  let y

  if (!center) {
    x = at.offsetX
    if (x !== undefined) {
      y = at.offsetY
    } else {
      x = at.clientX - rect.left
      y = at.clientY - rect.top
    }
  } else {
    x = rect.width / 2
    y = rect.height / 2
  }

  let ripple = document.createElement('div')
  let max
  if (rect.width === rect.height) {
    max = rect.width * 1.412
  } else {
    max = Math.sqrt(rect.width * rect.width + rect.height * rect.height)
  }
  let dim = max * 2 + 'px'
  ripple.style.width = dim
  ripple.style.height = dim
  ripple.style.marginLeft = -max + x + 'px'
  ripple.style.marginTop = -max + y + 'px'

  // Activate/add the element.
  ripple.className = 'ripple'

  holder.appendChild(ripple)
  window.setTimeout(function() {
    ripple.classList.add('held')
  }, 0)

  let releaseEvent = type === 'mousedown' ? 'mouseup' : 'touchend'
  if (!document.contains(holder)) return false

  function release() {
    document.removeEventListener(releaseEvent, release)
    ripple.classList.add('done')
    if (!document.contains(holder)) return false
    window.setTimeout(function() {
      if (!document.contains(holder)) return false
      if (holder.contains(ripple)) holder.removeChild(ripple)
      if (!holder.children.length) {
        cl.remove('active')
        holder.removeAttribute(rippleTypeAttr)
      }
    }, duration + 250)
  }
  document.addEventListener(releaseEvent, release)
}

function initRipple(node, params) {
  const { center, duration, color } = {
    center: false,
    duration: 400,
    color: 'currentColor',
    ...params,
  }

  let rippleNode = document.createElement('div')
  rippleNode.classList.add('rippleJS')
  rippleNode.style.setProperty('--ripple-duration', duration + 'ms')
  rippleNode.style.setProperty('--ripple-color', color)

  node.style.position = 'relative'
  node.style.overflow = 'visible'
  node.append(rippleNode)

  function handleMouseDown(ev) {
    if (ev.button === 0) {
      try {
        startRipple(ev.type, ev, rippleNode, center, duration)
      } catch (err) {}
    }
  }

  function handleTouchStart(ev) {
    for (let i = 0; i < ev.changedTouches.length; ++i) {
      try {
        startRipple(ev.type, ev.changedTouches[i], rippleNode, center, duration)
      } catch (err) {}
    }
  }

  node.addEventListener('mousedown', handleMouseDown, { passive: true })
  node.addEventListener('touchstart', handleTouchStart, { passive: true })

  return {
    destroy() {
      node.removeChild(rippleNode)
      node.removeEventListener('mousedown', handleMouseDown)
      node.removeEventListener('touchstart', handleTouchStart)
    },
  }
}

export default initRipple
